# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import datetime

import dateutil.parser
import django.forms

import th_django.forms.widgets


class SubjectID(django.forms.CharField):
    def __init__(self, id_regex, **kwargs):
        self._id_regex = id_regex
        super().__init__(**kwargs)

    def widget_attrs(self, widget):
        if isinstance(widget, django.forms.widgets.TextInput):
            return {
                'pattern': self._id_regex
            }


class DateField(django.forms.DateField):
    def __init__(self, years=None, common_html_class=None, default=datetime.date.today, **kwargs):
        if not years:
            this_year = datetime.date.today().year
            years = range(this_year - 10, this_year + 10)

        attrs = {}
        if common_html_class:
            attrs['class'] = common_html_class

        kwargs['widget'] = th_django.forms.widgets.SelectDateWidget(years=years, attrs=attrs)

        if 'initial' not in kwargs or not kwargs['initial']:
            kwargs['initial'] = default

        super().__init__(**kwargs)


class DateTimeField(django.forms.DateTimeField):
    def __init__(self, years=None, common_html_class=None, **kwargs):
        if not years:
            this_year = datetime.date.today().year
            years = range(this_year - 10, this_year + 10)

        attrs = {}
        if common_html_class:
            attrs['class'] = common_html_class

        kwargs['widget'] = th_django.forms.widgets.SelectDateTimeWidget(years=years, attrs=attrs)

        super().__init__(**kwargs)

    def to_python(self, value):
        value = dateutil.parser.parse('T'.join(value))
        return super().to_python(value)
