# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
from django import forms
from .selecttimewidget import SelectTimeWidget

class ModelAsStringWidget(forms.TextInput):
    def __init__(self, model, attrs=None):
        self._model = model
        super().__init__(attrs)

    def render(self, name, value, attrs=None, renderer=None):
        new_value = str(self._model.objects.get(pk=value))
        return super().render(name, new_value, attrs, renderer)


class SelectDateTimeWidget(forms.MultiWidget):
    def __init__(self, years, attrs, **kwargs):
        widgets = (forms.SelectDateWidget(years=years, attrs=attrs), SelectTimeWidget(attrs=attrs, use_seconds=False))

        super().__init__(widgets, **kwargs)

    def decompress(self, value):
        if value:
            return [value.date(), value.time().replace(microsecond=0)]
        return [None, None]

    def get_context(self, name, value, attrs):

        if 'class' not in attrs:
            attrs['class'] = ''

        attrs['class'] += ' selectdatetime_%s' % (name, )
        context = super().get_context(name, value, attrs)
        return context

class SelectDateWidget(forms.SelectDateWidget):
    def get_context(self, name, value, attrs):

        if 'class' not in attrs:
            attrs['class'] = ''

        attrs['class'] += ' selectdate_%s' % (name, )
        context = super().get_context(name, value, attrs)
        return context
