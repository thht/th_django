# taken from https://github.com/sean-wallace/django-select-time-widget

import datetime
import re

from django.forms.widgets import Widget, Select
from django.utils.formats import get_format

__all__ = ('SelectTimeWidget',)

# Attempt to match many time formats:
# Example: "12:34:56 P.M."  matches:
# ('12', '34', ':56', '56', 'P.M.', 'P', '.', 'M', '.')
# ('12', '34', ':56', '56', 'P.M.')
# Note that the colon ":" before seconds is optional, but only if seconds are omitted
time_pattern = r'(\d\d?):(\d\d)(:(\d\d))? *([aApP]\.?[mM]\.?)?$'

RE_TIME = re.compile(time_pattern)
# The following are just more readable ways to access re.matched groups:
HOURS = 0
MINUTES = 1
SECONDS = 3


class SelectTimeWidget(Widget):
    """
    A Widget that splits time input into <select> elements.
    Allows form to show as 24hr: <hour>:<minute>:<second>, (default)
    or as 12hr: <hour>:<minute>:<second> <am|pm>

    Also allows user-defined increments for minutes/seconds
    """
    hour_field = '%s_hour'
    minute_field = '%s_minute'
    second_field = '%s_second'
    use_seconds = True
    select_widget = Select

    template_name = 'django/forms/widgets/multiwidget.html'
    input_type = 'select'

    def __init__(self, attrs=None, hour_step=None, minute_step=None, second_step=None, use_seconds=True):
        """
        hour_step, minute_step, second_step are optional step values for
        for the range of values for the associated select element
        twelve_hr: If True, forces the output to be in 12-hr format (rather than 24-hr)
        use_seconds: If False, doesn't show seconds select element and stores seconds = 0.
        """
        self.attrs = attrs or {}

        if hour_step:  # 24hr, with stepping.
            self.hours = range(0, 24, hour_step)
        else:  # 24hr, no stepping
            self.hours = range(0, 24)

        if minute_step:
            self.minutes = range(0, 60, minute_step)
        else:
            self.minutes = range(0, 60)

        if second_step:
            self.seconds = range(0, 60, second_step)
        else:
            self.seconds = range(0, 60)

        self.use_seconds = use_seconds

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        time_context = {}

        hour_choices = [("%.2d" % i, "%.2d" % i) for i in self.hours]
        hour_attrs = context['widget']['attrs'].copy()
        hour_name = self.hour_field % name
        hour_attrs['id'] = 'id_%s' % hour_name
        time_context['hour'] = self.select_widget(attrs, choices=hour_choices).get_context(
            name=hour_name,
            value=context['widget']['value']['hour'],
            attrs=hour_attrs
        )

        minute_choices = [("%.2d" % i, "%.2d" % i) for i in self.minutes]
        minute_attrs = context['widget']['attrs'].copy()
        minute_name = self.minute_field % name
        minute_attrs['id'] = 'id_%s' % minute_name
        time_context['minute'] = self.select_widget(attrs, choices=minute_choices).get_context(
            name=minute_name,
            value=context['widget']['value']['minute'],
            attrs=minute_attrs
        )

        second_choices = [("%.2d" % i, "%.2d" % i) for i in self.seconds]
        second_attrs = context['widget']['attrs'].copy()
        second_name = self.second_field % name
        second_attrs['id'] = 'id_%s' % second_name
        time_context['second'] = self.select_widget(attrs, choices=second_choices).get_context(
            name=second_name,
            value=context['widget']['value']['second'],
            attrs=second_attrs
        )

        subwidgets = []
        field_order = ['hour', 'minute']
        if self.use_seconds:
            field_order.append('second')

        for field in field_order:
            subwidgets.append(time_context[field]['widget'])

        context['widget']['subwidgets'] = subwidgets

        return context

    def id_for_label(self, id_):
        return '%s_hour' % id_

    id_for_label = classmethod(id_for_label)

    def format_value(self, value):
        hour, minute, second = None, None, None
        if isinstance(value, (datetime.time, datetime.datetime)):
            hour, minute, second = value.hour, value.minute, value.second
        elif isinstance(value, str):
            for input_format in get_format('TIME_INPUT_FORMATS'):
                try:
                    cur_time = datetime.datetime.strptime(value, input_format)
                except ValueError:
                    pass
                else:
                    hour, minute, second = cur_time.hour, cur_time.minute, cur_time.second

        return {'hour': hour, 'minute': minute, 'second': second}

    def value_from_datadict(self, data, files, name):
        # if there's not h:m:s data, assume zero:
        h = data.get(self.hour_field % name, 0)  # hour
        m = data.get(self.minute_field % name, '00')  # minute
        s = data.get(self.second_field % name, '00')  # second

        if (int(h) == 0 or h) and m and s:
            return '%s:%s:%s' % (h, m, s)

        return data.get(name, None)
