# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import django.utils.timezone
from django.core.exceptions import ObjectDoesNotExist, ImproperlyConfigured
from django.http import Http404
from django.http import HttpResponseRedirect
from django.utils.encoding import force_text
from django.views.generic.base import View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import UpdateView, DeleteView
from extra_views import UpdateWithInlinesView

from th_django.views.mixins import TrashcanMixin


class CreateUpdateView(UpdateView):
    create_if_nonexistent = False

    def get_object(self, queryset=None):
        if self.slug_field in self.kwargs or self.pk_url_kwarg in self.kwargs:
            try:
                obj = super().get_object(queryset)
            except Http404 as e:
                if self.create_if_nonexistent:
                    return None
                else:
                    raise e

            return obj

        elif self.create_if_nonexistent:
            return self.model()

        else:
            return None


class CreateUpdateWithInlineView(UpdateWithInlinesView):
    create_if_nonexistent = False

    def get_object(self, queryset=None):
        if self.slug_field in self.kwargs or self.pk_url_kwarg in self.kwargs:
            try:
                obj = super().get_object(queryset)
            except Http404 as e:
                if self.create_if_nonexistent:
                    return None
                else:
                    raise e

            return obj

        elif self.create_if_nonexistent:
            return self.model()

        else:
            return None


class CreateUpdateTrashcanView(CreateUpdateView, TrashcanMixin):
    def get_object(self, queryset=None):
        if self.slug_field in self.request.POST:
            query = dict()
            query[self.slug_field] = self.request.POST[self.slug_field]
            try:
                this_object = self.model.objects.get(**query)
            except ObjectDoesNotExist:
                return super().get_object()
            setattr(this_object, self.deleted_field, False)
            setattr(this_object, self.deleted_datetime_field, None)
            return this_object
        else:
            return super().get_object()


class CreateUpdateWithInlineTrashcanView(CreateUpdateWithInlineView, TrashcanMixin):
    def get_object(self, queryset=None):
        if self.slug_field in self.request.POST:
            query = dict()
            query[self.slug_field] = self.request.POST[self.slug_field]
            try:
                this_object = self.model.objects.get(**query)
            except ObjectDoesNotExist:
                return super().get_object()
            setattr(this_object, self.deleted_field, False)
            setattr(this_object, self.deleted_datetime_field, None)
            return this_object
        else:
            return super().get_object()


class CreateWithForeignKeyView(UpdateView):
    foreign_model = None
    foreign_model_field = None
    parameter_url_kwarg = None
    form_field = None
    reverse_field = None

    def __init__(self, **kwargs):
        if not self.reverse_field:
            self.reverse_field = self.model._meta.model_name

        super().__init__(**kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['foreign_object'] = self.foreign_object

        return context

    def form_valid(self, form):
        setattr(form.instance, self.form_field, self.foreign_object)
        return super().form_valid(form)

    @property
    def foreign_object(self):
        query = {'%s__exact' % (self.foreign_model_field,): self.kwargs[self.parameter_url_kwarg]}
        return self.foreign_model.objects.get(**query)

    def get_object(self, queryset=None):
        try:
            return getattr(self.foreign_object, self.reverse_field)
        except (ObjectDoesNotExist, AttributeError):
            return None


class DeleteToTrashcanView(DeleteView, TrashcanMixin):
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        if not self.deleted_field:
            raise ValueError('The deleted_field field must not be None')

        setattr(self.object, self.deleted_field, True)

        if self.deleted_datetime_field:
            setattr(self.object, self.deleted_datetime_field, django.utils.timezone.now())

        self.object.save()

        return HttpResponseRedirect(success_url)


class UndeleteFromTrashcanView(SingleObjectMixin, View, TrashcanMixin):
    success_url = None

    def get_success_url(self):
        """
        Returns the supplied success URL.
        """
        if self.success_url:
            # Forcing possible reverse_lazy evaluation
            url = force_text(self.success_url)
        else:
            raise ImproperlyConfigured(
                "No URL to redirect to. Provide a success_url.")
        return url

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        setattr(self.object, self.deleted_field, False)
        setattr(self.object, self.deleted_datetime_field, None)
        self.object.save()

        return HttpResponseRedirect(success_url)
