# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import inspect
import os.path
import uuid

from django.conf.urls import url
from django.contrib import messages

from th_django.views import urlmixin_kwargs
from ..settings import TH_DJANGO_Settings
from ..utils.webassetsrjs import make_bundle


class TrashcanMixin:
    deleted_field = 'deleted'
    deleted_datetime_field = 'datetime_deleted'


class MessageAfterPostMixin:
    message_text = None
    message_priority = messages.INFO

    def get_message_text(self):
        return self.message_text

    def get_message_priority(self):
        return self.message_priority

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        if self.get_message_text():
            messages.add_message(request, self.get_message_priority(), self.get_message_text())

        return response


class AutoUrlMixin:
    url_path_name = None
    url_kwargs = None
    url_django_name = None
    is_index = False
    url_ignore_pk = False

    @classmethod
    def get_url_path_name(cls):
        if cls.url_path_name:
            return cls.url_path_name
        else:
            return cls.__name__

    @classmethod
    def get_url_kwargs(cls):
        url_kwargs = cls.url_kwargs
        if not url_kwargs and hasattr(cls, 'pk_url_kwarg') and not cls.url_ignore_pk:
            if cls.pk_url_kwarg == 'pk':
                url_kwargs = [urlmixin_kwargs.int('pk')]
            else:
                url_kwargs = [urlmixin_kwargs.string(cls.pk_url_kwarg)]

        if not url_kwargs and hasattr(cls, 'slug_url_kwarg') and not cls.url_ignore_pk:
            url_kwargs = [urlmixin_kwargs.string(cls.slug_url_kwarg)]

        return url_kwargs

    @classmethod
    def get_url_django_name(cls):
        if cls.url_django_name:
            return cls.url_django_name
        else:
            return cls.__name__

    @classmethod
    def get_url(cls):
        if cls.is_index:
            url_part = r'^$'
            return url(url_part, cls.as_view(), name=cls.get_url_django_name())
        else:
            return cls._create_url(cls.get_url_path_name(), cls.get_url_kwargs(), cls.get_url_django_name())

    @classmethod
    def _create_url(cls, path_name, url_kwargs, django_name):
        url_part = r'^%s/' % (path_name,)

        if url_kwargs:
            for cur_kwarg in url_kwargs:
                url_part = url_part + r'%s/' % (cur_kwarg.get_regex())

        url_part = url_part + r'$'

        return url(url_part, cls.as_view(), name=django_name)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class MultiAutoUrlMixin(AutoUrlMixin):
    @classmethod
    def get_url_django_name(cls):
        cur_django_name = super().get_url_django_name()
        if not isinstance(cur_django_name, list):
            return [cur_django_name] * len(cls.get_url_kwargs())
        else:
            if len(cur_django_name) != len(cls.get_url_kwargs()):
                raise ValueError('You must supply as many url_django_name as kwargs')
            else:
                return cur_django_name

    @classmethod
    def get_url_path_name(cls):
        cur_path_name = super().get_url_path_name()
        if not isinstance(cur_path_name, list):
            return [cur_path_name] * len(cls.get_url_kwargs())
        else:
            if len(cur_path_name) != len(cls.get_url_kwargs()):
                raise ValueError('You must supply as many url_path_names as kwargs')
            else:
                return cur_path_name

    @classmethod
    def get_url(cls):
        if cls.is_index:
            raise (ValueError, 'is_index cannot be true if you ise MultiAutoUrlMixin')

        # zip all parameters...
        all_urls = list()
        for cur_path_name, cur_kwargs, cur_django_name in zip(cls.get_url_path_name(), cls.get_url_kwargs(),
                                                              cls.get_url_django_name()):
            all_urls.append(cls._create_url(cur_path_name, cur_kwargs, cur_django_name))

        return all_urls


class WebAssetRequireJSMixin:
    webasset_bundle_name = None
    webasset_js_file = None

    @classmethod
    def _get_complete_classname(cls):
        return '.'.join([inspect.getmodule(cls).__name__, cls.__name__])

    @classmethod
    def _get_webasset_output_name(cls):
        these_settings = TH_DJANGO_Settings()

        if these_settings.WEBASSETS_SCRAMBLE_OUT_JS:
            return '.'.join([str(uuid.uuid4()), 'js'])
        else:
            return '.'.join([cls._get_complete_classname(), 'js'])

    @classmethod
    def get_webasset_bundle_name(cls):
        if cls.webasset_bundle_name:
            return cls.webasset_bundle_name
        else:
            return cls._get_complete_classname()

    @classmethod
    def get_webasset_js_file(cls):
        if cls.webasset_js_file:
            return cls.webasset_js_file

    @property
    def full_webasset_path(self):
        these_settings = TH_DJANGO_Settings()
        js_path = these_settings.WEBASSETS_JS_FOLDER

        if self.get_webasset_js_file():
            return os.path.join(js_path, self.get_webasset_js_file())
        else:
            return None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.full_webasset_path:
            make_bundle(self.full_webasset_path, self._get_webasset_output_name(), self.get_webasset_bundle_name())
