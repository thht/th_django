# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.
import inspect

import django_assets.env
from django import template
from django.template import TemplateSyntaxError
from django.template.base import kwarg_re
from django.template.defaulttags import URLNode
from django.urls import reverse_lazy
from django.utils import formats

import th_django.views.mixins
from th_django.utils.urls import Class2URLName
from ..utils.webassetsrjs import make_bundle

register = template.Library()

input_formats = formats.get_format_lazy('DATE_INPUT_FORMATS')
input_formats_datetime = formats.get_format_lazy('DATETIME_INPUT_FORMATS')


def _get_complete_classname(cls):
    return '.'.join([inspect.getmodule(cls).__name__, cls.__name__])


class custom_view_name(object):
    def __init__(self, view_class):
        self.view_class = view_class

    def resolve(self, context):
        return reverse_lazy(self.view_class)


@register.simple_tag
def date_format(for_javascript=False):
    if not for_javascript:
        return input_formats[0]

    dateformat = input_formats[0]
    dateformat = dateformat.replace('%Y', 'yyyy').replace('%m', 'mm').replace('%d', 'dd')

    return dateformat


@register.simple_tag
def datetime_format(for_javascript=False):
    if not for_javascript:
        return input_formats_datetime[0]

    dateformat = input_formats_datetime[0]
    dateformat = dateformat.replace('%Y', 'yyyy').replace('%m', 'mm').replace('%d', 'dd').replace('%H', 'hh').replace(
        '%M', 'ii').replace('%S', 'ss')

    return dateformat


@register.tag
def url_from_class(parser, token):
    bits = token.split_contents()
    if len(bits) < 2:
        raise TemplateSyntaxError("'%s' takes at least one argument, the name of a url()." % bits[0])

    tmp_name = "'%s'" % (Class2URLName.get_lookup_class_urlname(bits[1][1:-1]),)
    viewname = parser.compile_filter(tmp_name)

    args = []
    kwargs = {}
    asvar = None
    bits = bits[2:]
    if len(bits) >= 2 and bits[-2] == 'as':
        asvar = bits[-1]
        bits = bits[:-2]

    if len(bits):
        for bit in bits:
            match = kwarg_re.match(bit)
            if not match:
                raise TemplateSyntaxError("Malformed arguments to url tag")
            name, value = match.groups()
            if name:
                kwargs[name] = parser.compile_filter(value)
            else:
                args.append(parser.compile_filter(value))

    return URLNode(viewname, args, kwargs, asvar)


@register.inclusion_tag('webassets_tag.html', takes_context=True)
def webassets_js(context, default_js = None):
    view_class = context['view'].__class__
    view_class_name = _get_complete_classname(view_class)
    if isinstance(context['view'], th_django.views.mixins.WebAssetRequireJSMixin) and view_class_name in django_assets.env.get_env():
        return {'full_class_name': view_class_name}
    elif default_js:
        make_bundle(default_js, '.'.join([view_class_name, 'js']), view_class_name)
        return {'full_class_name': view_class_name}

