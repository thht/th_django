# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import inspect

import django.urls
from django.urls import URLPattern, URLResolver

from th_django.utils.lazy import lazy
from th_django.views.mixins import AutoUrlMixin

url_cache = dict()


def get_urls_from_package(package):
    # collect all modules defined directly in that package...
    package_classes = [mod[1] for mod in inspect.getmembers(package, inspect.isclass) if
                       mod[1].__module__ == package.__name__]
    url_classes = [cls for cls in package_classes if issubclass(cls, AutoUrlMixin)]

    all_urls = list()
    for cur_class in url_classes:
        url_cache['.'.join([inspect.getmodule(cur_class).__name__, cur_class.__name__])] = cur_class
        rval = cur_class.get_url()
        if isinstance(rval, list):
            all_urls.extend(rval)
        else:
            all_urls.append(rval)

    return all_urls


class Class2URLName(object):
    url_name_cache = None

    @classmethod
    def get_lookup_class_urlname(cls, class_name):
        if not cls.url_name_cache:
            cls.generate_url_names_cache()

        if isinstance(class_name, type):
            class_name = '.'.join([inspect.getmodule(class_name).__name__, class_name.__name__])

        if not isinstance(class_name, str):
            raise ValueError('You must supply either a string or a class')

        try:
            url = cls.url_name_cache[class_name]
        except KeyError:
            raise ValueError('Supplied class name "%s" does not match any view classes' % (class_name,))

        return url

    @classmethod
    def generate_url_names_cache(cls):
        cls.url_name_cache = cls.parse_urls(django.urls.get_resolver().url_patterns)

    @classmethod
    def parse_urls(cls, patterns):
        all_patterns = {}
        if isinstance(patterns, list):
            for cur_pattern in patterns:
                all_patterns.update(cls.parse_urls(cur_pattern))

        if isinstance(patterns, URLResolver):
            cur_namespace = patterns.namespace
            cur_app = patterns.app_name
            all_items = {}
            for cur_pattern in patterns.url_patterns:
                all_items.update(cls.parse_urls(cur_pattern))

            for cur_key, cur_item in all_items.items():
                if not cur_namespace:
                    all_items[cur_key] = '%s' % (cur_item,)
                else:
                    all_items[cur_key] = '%s:%s' % (cur_namespace, cur_item)

            return all_items

        if isinstance(patterns, URLPattern):
            return {patterns.lookup_str: patterns.name}

        return all_patterns


def reverse_classname(class_name):
    return Class2URLName.get_lookup_class_urlname(class_name)


reverse_classname_lazy = lazy(reverse_classname, str)


def reverse_local_classname(class_name, lazy=True):
    cur_frame = inspect.currentframe()
    class_name = '.'.join([cur_frame.f_back.f_locals['__module__'], class_name])

    if lazy:
        return reverse_classname_lazy(class_name)
    else:
        return reverse_classname(class_name)
