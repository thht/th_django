# Copyright (c) 2016-2018, Thomas Hartmann
#
# This file is part of the OBOB Subject Database Project, see: https://gitlab.com/obob/obob_subjectdb/
#
#    obob_subjectdb is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obob_subjectdb is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obob_subjectdb. If not, see <http://www.gnu.org/licenses/>.

import importlib
import logging
import os.path
import sys

import django.urls
from django.core.management.base import BaseCommand, CommandError
from django_assets.env import get_env
from webassets.script import (CommandError as AssetCommandError,
                              GenericArgparseImplementation)

from th_django.utils.urls import Class2URLName
from ...utils.webassetsrjs import WebassetsRjsLoader
from ...views.mixins import WebAssetRequireJSMixin


class Command(BaseCommand):
    help = 'Build assets defined by the WebAssetRequireJSMixin mixin.'
    requires_system_checks = True

    def handle(self, *args, **options):
        log = logging.getLogger('th_utils-assets')
        log.setLevel({0: logging.WARNING, 1: logging.INFO, 2: logging.DEBUG}[int(options.get('verbosity', 1))])
        log.addHandler(logging.StreamHandler())

        dummy = django.urls.get_resolver().url_patterns
        view_class_names = Class2URLName.parse_urls(dummy).keys()
        for cur_class_name in view_class_names:
            (cur_package, cur_module) = cur_class_name.rsplit('.', 1)
            cur_class = getattr(importlib.import_module(cur_package), cur_module)
            if issubclass(cur_class, WebAssetRequireJSMixin):
                dummy_obj = cur_class()


        get_env().add(*[b for b in self.load_from_templates()
                        if not b.is_container])

        prog = "%s assets" % os.path.basename(sys.argv[0])
        impl = GenericArgparseImplementation(
            env=get_env(), log=log, no_global_options=True, prog=prog)
        try:
            # The webassets script runner may either return None on success (so
            # map that to zero) or a return code on build failure (so raise
            # a Django CommandError exception when that happens)
            retval = impl.run_with_argv(('clean',)) or 0
            retval = impl.run_with_argv(('build',)) or 0
            if retval != 0:
                raise CommandError('The webassets build script exited with '
                                   'a non-zero exit code (%d).' % retval)
        except AssetCommandError as e:
            raise CommandError(e)

    def load_from_templates(self):
        # Using the Django loader
        bundles = WebassetsRjsLoader().load_bundles()

        return bundles